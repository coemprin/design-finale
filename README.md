# KeyFinder Version finale



## Ajout d'un capteur : Un microphone

L'ajout d'un microphone permettrait au KeyFinder de détecter les sons qui l'entoure. Cela pourrait avoir plusieurs utilités :
-Ces sons pourrait être rejouer depuis le téléphone pour donner un indice supplémentaire sur l'entourage de l'objet.
-L'utilisateur pourrait faire un bruit significatif (taper 2-3 fois dans ses mains, siffler) qui déclencherait le buzzer et la led du KeyFinder permettant de le localiser plus facilement.

## Démarche de recherche:

Différents types de microphone : analogiques et numériques 

J'ai selectionné un microphone numérique peu cher avec une bonne disponibilité.

Lien: https://www.digikey.fr/fr/products/detail/knowles/SPH0644LM4H-1/8573344

## Remarque

Pour ajouter le micro, il a fallut rajouter un transistor et une résistance. Un des problèmes pourrait être la taille du micro. Il est possible qu'il ne puisse pas rentrer sous la coque du KeyFinder.

Je souhaitais aussi rajouter une antenne 2.4GHz pour établir la connexion Bluetooth mais je manquais de place: j'aurais du placer ce composant en premier puisqu'il doit le plus proche possible du la puce nrf452. L'antenne ne peut être placée à l'autre bout ou de l'autre coté  de la carte: cela provoquerait trop d'interférences.
